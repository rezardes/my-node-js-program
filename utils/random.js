const getRandomIdx = (data) => (Math.floor(Math.random() * data.length));

module.exports = getRandomIdx;