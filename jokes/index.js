const axios = require('axios');
const getRandomIdx = require('../utils/random');

var jokes = []

const fetchJoke = async () => {
    return await axios.get('http://api.icndb.com/jokes/random');
};

const fetchNJokes = async (n) => {
    for(var i=0;i<n;i++)
    {
        fetchJoke()
        .then(res => {
            if (jokes.some(data => data.id == res.data.value.id)) {
                i--;
            } else {
                jokes.push(res.data.value);
            }
        })
        .catch(err => {
            console.log(err);
        })
    }
}

const getRandomJokes = (n) => {
    var temp = [];
    var indices = [];
    n = jokes.length < n ? jokes.length : n;
    for(var i=0;i<n;i++)
    {
        randomIdx = getRandomIdx(jokes);
        while(indices.some(data => data == randomIdx)) {
            randomIdx = getRandomIdx(jokes);
        }
        
        indices.push(randomIdx);
        const item = jokes[randomIdx];
        temp.push(item);
    }

    return temp;
}

const freqWordJokes = () => {
    var temp = getRandomJokes(10);
    var words = {};
    temp = temp.map(data => data.joke);
    temp.forEach(data => {
        var tempWords = data.split(" ");
        tempWords.forEach(word => {
            word = word.toLowerCase();
            word = word.replace(/[^a-zA-Z0-9]/i, "");

            var isEmptyString = word == "";
            
            if (word in words && !isEmptyString) {
                words[word] = words[word] + 1;
            } else if (!isEmptyString) {
                words[word] = 1;
            }
        });
    })

    return {
        jokes: temp,
        words
    }
}

const removeJokes = () => {
    jokes = [];
    return jokes;
}

const fetch10Jokes = async () => {
    await fetchNJokes(10);
    return jokes;
}

const initJokes = async () => {
    await fetchNJokes(10); //
}

module.exports = {
    jokes,
    initJokes,
    fetch10Jokes,
    freqWordJokes,
    getRandomJokes,
    removeJokes
}